package com.yast;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SoftwareStackTestApplication.class)
@WebAppConfiguration
public class SoftwareStackTestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
