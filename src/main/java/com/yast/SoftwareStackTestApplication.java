package com.yast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan({"demo","controller"})
public class SoftwareStackTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftwareStackTestApplication.class, args);
	}
}
