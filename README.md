# README #

Software Project Management Architecture Template

### What is this repository for? ###

* Boilerplate for project architecture
* 0.1.0

### Project Software Stack ###
* Spring Boot with Spring MVC, Tomcat, Maven & Thymeleaf
* mongodb or postgresql (TDB)
* Bootstrap 3.3.6
* JQuery 2.2.0
* Java 1.8

### Requirements? ###
Spring Tool Suite 3.7.2 (Based on Eclipse), or STS plugin for Eclipse.
Both can be found at http://spring.io/tools/sts/all

Other IDEs can be used as well, but you'll have to make sure they're set up to
work with Spring.

### How do I get set up? ###

* Clone this project
* Open either STS or Eclipse with STS plugin
* STS & Eclipse: Select File/Import/Existing Projects into Workspace, and choose the root directory for the project
* softwareStackTest project should appear in Package Explorer
* Select project, and select Run As 'Spring Boot App'
* Wait until console states "Started SoftwareTestApplication in...."
* Open browser to localhost:8080. Can also browse to localhost:8080/person